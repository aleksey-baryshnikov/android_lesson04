package thirdapplication.main.second.stopwatcher;

import android.os.Handler;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private int seconds = 0;
    private boolean isRunning = false;
    private boolean wasRunning = false;

    private Button buttonStart;
    private Button buttonPause;
    private Button buttonReset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //tooltip("created");

        if (savedInstanceState != null) {
            seconds = savedInstanceState.getInt("seconds");
            isRunning = savedInstanceState.getBoolean("isRunning");
            wasRunning = savedInstanceState.getBoolean("wasRunning");
        }

        initButtons();
        runTimer();
    }

    private void initButtons() {
        buttonStart = findViewById(R.id.buttonStart);
        buttonPause = findViewById(R.id.buttonPause);
        buttonReset = findViewById(R.id.buttonReset);

        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isRunning = true;
            }
        });

        buttonPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isRunning = false;
            }
        });

        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isRunning = false;
                seconds = 0;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        isRunning = wasRunning;
    }

    @Override
    protected void onPause() {
        super.onPause();
        wasRunning = isRunning;
        isRunning = false;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("seconds", seconds);
        outState.putBoolean("isRunning", isRunning);
        outState.putBoolean("wasRunning", wasRunning);
    }

    private void runTimer() {

        final TextView textSeconds = findViewById(R.id.textSeconds);
        final Handler handler = new Handler();

        handler.post(new Runnable() {
            @Override
            public void run() {
                int hours = seconds / 60 / 60;
                int minutes = seconds % 3600 / 60;
                int sec = seconds % 60;
                String time = String.format(Locale.getDefault(), "%02d:%02d:%02d", hours, minutes, sec);
                textSeconds.setText(time);

                if (sec == 10) {
                    tooltip("Time is over");
                }

                seconds++;
                /*
                if (isRunning) {
                    seconds++;
                }*/
                
                handler.postDelayed(this, 1000);
            }
        });
    }

    /*
    @Override
    protected void onStart() {
        super.onStart();
        tooltip("started");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        tooltip("restarted");
    }

    @Override
    protected void onResume() {
        super.onResume();
        tooltip("resumed");
    }

    @Override
    protected void onPause() {
        super.onPause();
        tooltip("paused");
    }

    @Override
    protected void onStop() {
        super.onStop();
        tooltip("stopped");
    }

*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tooltip("destroyed");
    }

    private void tooltip(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
